Feature: Update product lists

  Scenario: User updates the product lists
    Given the user inputs the row ID of a product
    And inputs the new product "name", "quantity", and "price"
    When the inputted row ID matches with one of the product
    Then replace the product with the new product provided by the user.
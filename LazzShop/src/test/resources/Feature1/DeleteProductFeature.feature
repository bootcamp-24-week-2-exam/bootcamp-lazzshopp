Feature: Delete product

  Scenario: User deletes the product
    Given the user inputs row ID of 2
    When the inputted row ID matches in the database
    Then delete that row in the database
Feature: Check Product List

  Scenario: Check if the product list is empty or not
    Given the current product is "apple",
    And the quantity is 200
    And the price is at 20
    When check if they exist in the product list
    Then show results
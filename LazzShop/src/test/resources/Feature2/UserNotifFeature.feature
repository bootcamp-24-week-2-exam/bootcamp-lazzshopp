Feature: User Notification

  Scenario: Account notification after creating user account
    Given the client has inputted their "firstname" and "lastname"
    When the system registers the "firstname" and "lastname" inside the database
    Then the client gets a notification that his account has been created



Feature: Order Checkout

  Scenario: View the total amount of items in the cart to know how much will be the payment
    Given the client already added a list of "items" on the cart
    When the client decided to checkout
    And the client should also be able to see the list of "items" from the cart
    When they trigger "showItems"
    Then The client should also be able to see the number of "items" on the cart during "getNumberOfItems"

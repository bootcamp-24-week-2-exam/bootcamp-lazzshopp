package com.example.demo.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class UpdateProductStepDef {
    @Given("the user inputs the row ID of a product")
    public void theUserInputsTheRowIDOfAProduct() {
        System.out.println("User inputs the product row..");
    }

    @And("inputs the new product {string}, {string}, and {string}")
    public void inputsTheNewProductAnd(String arg0, String arg1, String arg2) {
        System.out.print("New user input: " + arg0 + ", " + arg1 + ", " + arg2);
    }

    @When("the inputted row ID matches with one of the product")
    public void theInputtedRowIDMatchesWithOneOfTheProduct() {
        System.out.println("Checking for matches..");
    }

    @Then("replace the product with the new product provided by the user.")
    public void replaceTheProductWithTheNewProductProvidedByTheUser() {
        System.out.println("Replaced by the new product.");
    }
}

package com.example.demo.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class ShowAllProductStepDev {
    @Given("the user chooses to view all the products")
    public void theUserChoosesToViewAllTheProducts() {
        System.out.println("The user chooses to view all the products");
    }

    @Then("show all the products from the product's list")
    public void showAllTheProductsFromTheProductSList() {
        System.out.println("Showing all the products");
    }
}

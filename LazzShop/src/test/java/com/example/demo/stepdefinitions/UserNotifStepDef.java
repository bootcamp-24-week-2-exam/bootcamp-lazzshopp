package com.example.demo.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserNotifStepDef {
    @Given("the client has inputted their {string} and {string}")
    public void theClientHasInputtedTheirAnd(String arg0, String arg1) {
        System.out.println("The client has inputted their " + arg0 + " and " + arg1);
    }

    @When("the system registers the {string} and {string} inside the database")
    public void theSystemRegistersTheAndInsideTheDatabase(String arg0, String arg1) {
        System.out.println("The system registers the " +  arg0 + " and " + arg1 + " inside the database");
    }

    @Then("the client gets a notification that his account has been created")
    public void theClientGetsANotificationThatHisAccountHasBeenCreated() {
        System.out.println("The client gets a notification that his account has been created");
    }
}

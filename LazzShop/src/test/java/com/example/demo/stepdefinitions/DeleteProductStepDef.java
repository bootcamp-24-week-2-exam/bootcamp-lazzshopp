package com.example.demo.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class DeleteProductStepDef {
    @Given("the user inputs row ID of {int}")
    public void theUserInputsRowIDOf(int arg0) {
//        check if ID is empty.
        if(arg0 != 0) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @When("the inputted row ID matches in the database")
    public void theInputtedRowIDMatchesInTheDatabase() {
        System.out.println("Checking for matches..");
    }

    @Then("delete that row in the database")
    public void deleteThatRowInTheDatabase() {
        System.out.println("Product has been deleted.");
    }
}

package com.example.demo.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OrderCheckoutStepDef {
    @Given("the client already added a list of {string} on the cart")
    public void theClientAlreadyAddedAListOfOnTheCart(String arg0) {
        System.out.println("the client already added a list of " + arg0 + " on the cart");
    }

    @When("the client decided to checkout")
    public void theClientDecidedToCheckout() {
        System.out.println("the client decided to checkout\"");
    }

    @And("the client should also be able to see the list of {string} from the cart")
    public void theClientShouldAlsoBeAbleToSeeTheListOfFromTheCart(String arg0) {
        System.out.println("The client should also be able to see the list of " + arg0 + "from the cart");
    }

    @When("they trigger {string}")
    public void theyTrigger(String arg0) {
        System.out.println("They trigger " + arg0);
    }

    @Then("The client should also be able to see the number of {string} on the cart during {string}")
    public void theClientShouldAlsoBeAbleToSeeTheNumberOfOnTheCartDuring(String arg0, String arg1) {
        System.out.println("The client should also be able to see the number of " + arg0  + " on the cart during " +  arg1);
    }
}

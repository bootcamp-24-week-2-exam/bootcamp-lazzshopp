package com.example.demo.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

public class CreateProductListStepDef {
    @Given("the user inputted {string} for the product")
    public void theUserInputtedForTheProduct(String arg0) {
//        check if empty.
        if (arg0 != null) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @And("the quantity is at {int} pieces")
    public void theQuantityIsAtPieces(int arg0) {
//        check if empty.
        if (arg0 != 0) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @And("the product price is at {int} pesos")
    public void theProductPriceIsAtPesos(int arg0) {
        //        check if empty.
        if (arg0 != 0) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @Then("store product in the database")
    public void storeProductInTheDatabase() {
        Assertions.assertTrue(true);
        System.out.println("Storing product in the database");
    }
}

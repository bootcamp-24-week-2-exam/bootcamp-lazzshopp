package com.example.demo.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class CheckProductStepDef {
    @Given("the current product is {string},")
    public void theCurrentProductIs(String arg0) {
        if (arg0 != null) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @And("the quantity is {int}")
    public void theQuantityIs(int arg0) {
        if (arg0 != 0) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @And("the price is at {int}")
    public void thePriceIsAt(int arg0) {
        if (arg0 != 0) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @When("check if they exist in the product list")
    public void checkIfTheyExistInTheProductList() {
        System.out.println("check if they exist in the product list");
    }

    @Then("show results")
    public void showResults() {
        System.out.println("show results");
    }
}

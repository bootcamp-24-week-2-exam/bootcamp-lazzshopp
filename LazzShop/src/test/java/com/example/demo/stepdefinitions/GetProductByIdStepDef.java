package com.example.demo.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class GetProductByIdStepDef {
    @Given("the user input is {int} as the row ID")
    public void theUserInputIsAsTheRowID(int arg0) {
        //        check if ID is empty.
        if(arg0 != 0) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

    @When("the user input matches with the row ID")
    public void theUserInputMatchesWithTheRowID() {
        System.out.println("Checking for matches..");
    }

    @Then("show the product that corresponds with the row ID")
    public void showTheProductThatCorrespondsWithTheRowID() {
        System.out.println("Showing product..");
    }
}
